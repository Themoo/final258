package com.nachiengmai.moo.final258;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by wacharapongnachiengmai on 5/18/16 AD.
 */
public class StudentDAO {
    SQLiteDatabase database;
    DbHelper dbHelper;
    Context context;

    public StudentDAO(Context context) {
        this.dbHelper = new DbHelper(context);
        this.database = dbHelper.getReadableDatabase();
        this.context = context;
    }

    public void close(){
        this.database.close();
    }

    public ArrayList<Students> findAll(){
        ArrayList<Students> studentses = new ArrayList<>();
        String sqlText = "SELECT * FROM students; ";
        Cursor cursor = this.database.rawQuery(sqlText, null);
        cursor.moveToFirst();
        Students students;
        while (!cursor.isAfterLast()){
            students = new Students();
            students.setStudentId(cursor.getString(0));
            students.setStudentName(cursor.getString(1));
            students.setAndroidPoint(cursor.getFloat(2));
            studentses.add(students);
            cursor.moveToNext();
        }
        return studentses;
    }
}
