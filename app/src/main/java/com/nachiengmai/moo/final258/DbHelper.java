package com.nachiengmai.moo.final258;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by wacharapongnachiengmai on 3/22/16 AD.
 */
public class DbHelper extends SQLiteOpenHelper {
    private static final String databaseName = "finalexam258.sqlite";
    private static final int databaseVersion = 1;
    Context myContext;


    public DbHelper(Context context) {

        super(context, databaseName, null, databaseVersion);
        this.myContext = context;

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d("Moo1", "Table All");
        //สร้างตาราง students
        String sqlText = "CREATE TABLE students (" +
                "student_id TEXT PRIMARY KEY, " +
                "student_name TEXT, " +
                "android_point REAL " +
                ");";
        db.execSQL(sqlText);

        //ใส่ข้อมูลเริ่มต้นในตาราง students
        sqlText = "INSERT INTO students (student_id, student_name, android_point) VALUES (" +
                "'5801001', 'โนบิตะ', 40);";
        db.execSQL(sqlText);
        sqlText = "INSERT INTO students (student_id, student_name, android_point) VALUES (" +
                "'5801002', 'โดเรมอน', 50);";
        db.execSQL(sqlText);
        sqlText = "INSERT INTO students (student_id, student_name, android_point) VALUES (" +
                "'5801003', 'ชิซุกะ', 82);";
        db.execSQL(sqlText);
        sqlText = "INSERT INTO students (student_id, student_name, android_point) VALUES (" +
                "'5801004', 'ซุเนโอะ', 76);";
        db.execSQL(sqlText);
        sqlText = "INSERT INTO students (student_id, student_name, android_point) VALUES (" +
                "'5801005', 'ไจแอนท์', 61);";
        db.execSQL(sqlText);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {


    }
}
