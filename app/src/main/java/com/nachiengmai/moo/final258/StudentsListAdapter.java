package com.nachiengmai.moo.final258;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by wacharapongnachiengmai on 5/18/16 AD.
 */
public class StudentsListAdapter extends BaseAdapter {
    private static Activity activity;
    private static LayoutInflater layoutInflater;
    private ArrayList<Students> studentses;

    public StudentsListAdapter(Activity activity, ArrayList<Students> studentses) {
        this.activity = activity;
        this.studentses = studentses;
        this.layoutInflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return this.studentses.size();
    }

    @Override
    public Students getItem(int position) {
        return this.studentses.get(position);
    }

    @Override
    public long getItemId(int position) {
        return Long.parseLong(this.studentses.get(position).getStudentId());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        v = layoutInflater.inflate(R.layout.student_list_layout, null);
        TextView studentIdTextview = (TextView) v.findViewById(R.id.student_id);
        TextView studentNameTextview = (TextView) v.findViewById(R.id.student_name);
        TextView androidPointTextview = (TextView) v.findViewById(R.id.android_point);
        TextView androidGradeTextview = (TextView) v.findViewById(R.id.android_grade);
        studentIdTextview.setText(String.valueOf(this.studentses.get(position).getStudentId()));
        studentNameTextview.setText(this.studentses.get(position).getStudentName());
        androidPointTextview.setText(String.valueOf(this.studentses.get(position).getAndroidPoint()));
        androidGradeTextview.setText(this.studentses.get(position).getAndroidGrade());
        return v;
    }
}
