package com.nachiengmai.moo.final258;

/**
 * Created by wacharapongnachiengmai on 5/18/16 AD.
 */
public class Students {
    private String studentId;
    private String studentName;
    private float androidPoint;

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public float getAndroidPoint() {
        return androidPoint;
    }

    public void setAndroidPoint(float androidPoint) {
        this.androidPoint = androidPoint;
    }

    public String getAndroidGrade(){
        return "I";
    }
}
